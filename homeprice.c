#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *h;
    h = fopen("homelistings.csv", "r");
    if( !h )
    {
        printf("Can't open homelistings.csv");
        exit(1);
    }
    
    int zip, mls, price, bed, bath, area;
    char addr[50];
    
    int cheapest = 1000000000;
    int expensive = 0;
    double average = 0;
    double total = 0;
    double counter = 0;
    
    while( fscanf( h, "%d, %d, %[^,], %d, %d, %d, %d", &zip, &mls, addr, &price, &bed, &bath, &area) != EOF )
    {
        if( price > expensive )
        {
            expensive = price;
        }
        
        if( price < cheapest )
        {
            cheapest = price;
        }
        
        total += price;
        counter++;
    }
    
    average = total / counter;
    
    printf( "%d %d %.0lf\n", cheapest, expensive, average );
    
    fclose(h);
}