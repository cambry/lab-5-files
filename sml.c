#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *h;
    h = fopen("homelistings.csv", "r");
    if( !h )
    {
        printf("Can't open homelistings.csv\n");
        exit(1);
    }
    
    /*FILE *s;
    s = fopen("small.txt", "w");
    if( !s )
    {
        printf("Can't open small.txt\n");
        exit(1);
    }
    
    FILE *m;
    m = fopen("med.txt", "w");
    if( !m )
    {
        printf("Can't open med.txt\n");
        exit(1);
    }
    
    FILE *l;
    l = fopen("large.txt", "w");
    if( !l )
    {
        printf("Can't open large.txt\n");
        exit(1);
    }*/
    
    int zip, mls, price, bed, bath, area;
    char addr[50];
    
    while( fscanf( h, "%d, %d, %[^,], %d, %d, %d, %d", &zip, &mls, addr, &price, &bed, &bath, &area) != EOF )
    {
        if( area < 1000 )
        {
            fprintf(s, "%s : %d\n", addr, area);
        }
        else if( area >= 1000 && area <= 2000 )
        {
            fprintf(m, "%s : %d\n", addr, area);
        }
        else if( area > 2000 )
        {
            fprintf(l, "%s : %d\n", addr, area);
        }
    }
}